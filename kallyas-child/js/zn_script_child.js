(function ($) {

    $(window).load(function () {

        // YOUR CONTENT HERE
        var isMobile = false;
        var menuFixed = false;

        function cartToMobile() {
            if (!isMobile) {
                isMobile = true;
                const main = $(".fxb-row.site-header-row.site-header-main");
                const woocart = $('.fxb-col.fxb.fxb-end-x.fxb-center-y.fxb-basis-auto.site-header-col-right.site-header-top-right');
                woocart.removeClass("fxb-end-x site-header-top-right site-header-col-right");
                woocart.addClass("fxb-start-x site-header-top-left site-header-col-left");
                main.prepend(woocart);
            }
        }

        function cartToDesktop() {
            if (isMobile) {
                isMobile = false;
                const main = $('.fxb-row.site-header-row.site-header-top');
                const woocart = $('.fxb-col.fxb.fxb-start-x.fxb-center-y.fxb-basis-auto.site-header-col-left.site-header-top-left');
                woocart.removeClass("fxb-start-x site-header-top-left site-header-col-left");
                woocart.addClass("fxb-end-x site-header-top-right site-header-col-right");
                main.prepend(woocart);
            }
        }

        function menuFix() {
            // mobile close button fix
            if ($('.zn_res_menu_go_back .zn_res_back_icon.glyphicon.glyphicon-chevron-left').length && !menuFixed) {
                menuFixed = true;
                $($('.zn_res_menu_go_back .zn_res_menu_go_back_link')[0]).text('Close')
                const menuClose = $($('.zn_res_menu_go_back .zn_res_back_icon.glyphicon.glyphicon-chevron-left')[0]);
                menuClose.removeClass("glyphicon-chevron-left");
                menuClose.addClass("glyphicon-remove");
            }
        }

        $('.product_list_widget li').contents().filter(function () {
            return this.nodeType == 3
        }).each(function () {
            this.textContent = this.textContent.replace(' – ', '');
        });
        if ($('.woocommerce-noreviews').length === 0) {
            $('.reviews_tab').attr('style', 'display: block !important');
        }

        // remove unused header
        $('.sh-component.topnav.navRight.topnav--log.topnav-no-sc.topnav-no-hdnav').remove();
        $('.fxb-col.fxb.fxb-start-x.fxb-center-y.fxb-basis-auto.site-header-col-left.site-header-top-left').remove();

        $(window).resize(function () {
            if ($(window).width() <= 767) {
                cartToMobile();
            } else {
                cartToDesktop();
            }
        });
        if ($(window).width() <= 767) {
            cartToMobile();
        }

        $(window).resize(function () {
            if ($(window).width() <= 992) {
                menuFix();
            }
        });
        if ($(window).width() <= 992) {
            menuFix();
        }
        
        // fizetes sikeressegenek kijelzese
        if(window.location.href.startsWith('https://cloudbase.hu/fizetes/order-received')) {
            window.location.href = 'https://cloudbase.hu/sikeres-fizetes/'
        }

        // fizetes sikertelensegenek kijelzese
        if(window.location.href.startsWith('https://cloudbase.hu/kosar/?cancel_order=true')) {
            window.location.href = 'https://cloudbase.hu/sikertelen-fizetes'
        }
    });

})(jQuery);